package view_component.lib_ohos.com.view_component.base_view;

import org.jetbrains.annotations.NotNull;

/**
 * This interface is used in order to bind ControllerComponent in to Custom View's lifecycle
 *
 * @param <VC> ViewComponent type
 */
interface ControllerLifeCycle<VC extends ViewComponent> {
    /**
     * We call this inside {@link ComponentDelegateImpl} class's constructor method, after Components instantiation
     *
     * @param viewComponent non null viewComponent
     */
    void onCreate(@NotNull VC viewComponent);
}
