package view_component.lib_ohos.com.view_component.base_view.layouts;


import ohos.agp.components.AttrSet;
import ohos.agp.components.DirectionalLayout;
import ohos.app.Context;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import view_component.lib_ohos.com.view_component.base_view.ComponentDelegate;
import view_component.lib_ohos.com.view_component.base_view.ComponentDelegateImpl;
import view_component.lib_ohos.com.view_component.base_view.ControllerComponent;
import view_component.lib_ohos.com.view_component.base_view.ViewComponent;

import static view_component.lib_ohos.com.view_component.base_view.ComponentDelegateImpl.create;

public abstract class ComponentLinearLayout<VC extends ViewComponent, CC extends ControllerComponent<VC>> extends DirectionalLayout implements ComponentDelegate<VC, CC> {

    /**
     * componentDelegate
     */
    private final ComponentDelegateImpl<VC, CC> componentDelegate;

    public ComponentLinearLayout(@NotNull Context context) {
        this(context, null);
    }

    public ComponentLinearLayout(@NotNull Context context, @Nullable AttrSet attrs) {
        this(context, attrs, null);
    }

    public ComponentLinearLayout(@NotNull Context context, @Nullable AttrSet attrs, String defStyleAttr) {
        super(context, attrs, defStyleAttr);

        DirectionalLayout.LayoutConfig config = new DirectionalLayout.LayoutConfig(
                DirectionalLayout.LayoutConfig.MATCH_PARENT, LayoutConfig.MATCH_CONTENT);
        setLayoutConfig(config);
        componentDelegate = create(this, context);
    }

    @Override
    public VC getViewComponent() {
        return componentDelegate.getViewComponent();
    }

    @Override
    public CC getControllerComponent() {
        return componentDelegate.getControllerComponent();
    }
}
