package view_component.lib_ohos.com.view_component.base_view;

import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.app.Context;

import org.jetbrains.annotations.NotNull;

/**
 * This class holds the parents view instance
 * You can extend this class and use with butterKnife bindings
 */
public class ViewComponent {

    /**
     * rootView
     */
    final Component rootView;

    /**
     * rootViewGroup
     */
    private ComponentContainer rootViewGroup;

    /**
     * 构造函数
     *
     * @param rootView rootView
     */
    public ViewComponent(@NotNull Component rootView) {
        this.rootView = rootView;
        if (rootView instanceof ComponentContainer) {
            rootViewGroup = (ComponentContainer) rootView;
        }
    }

    protected Context getContext() {
        return rootView.getContext();
    }

    public Component getRootView() {
        return rootView;
    }

    public ComponentContainer getRootViewGroup() {
        return rootViewGroup;
    }
}
