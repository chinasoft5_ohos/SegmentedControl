package view_component.lib_ohos.com.view_component.base_view.layouts;


import ohos.agp.components.AttrSet;
import ohos.agp.components.StackLayout;
import ohos.app.Context;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import view_component.lib_ohos.com.view_component.base_view.ComponentDelegate;
import view_component.lib_ohos.com.view_component.base_view.ComponentDelegateImpl;
import view_component.lib_ohos.com.view_component.base_view.ControllerComponent;
import view_component.lib_ohos.com.view_component.base_view.ViewComponent;

import static view_component.lib_ohos.com.view_component.base_view.ComponentDelegateImpl.create;

public abstract class ComponentFrameLayout<VC extends ViewComponent, CC extends ControllerComponent<VC>> extends StackLayout implements ComponentDelegate<VC, CC> {

    /**
     * componentDelegate
     */
    private final ComponentDelegateImpl<VC, CC> componentDelegate;

    public ComponentFrameLayout(@NotNull Context context) {
        this(context,null);
    }

    public ComponentFrameLayout(@NotNull Context context, @Nullable AttrSet attrs) {
        this(context, attrs,null);
    }

    public ComponentFrameLayout(@NotNull Context context, @Nullable AttrSet attrs, String defStyleAttr) {
        super(context, attrs, defStyleAttr);
        componentDelegate = create(this, context);
    }

    @Override
    public VC getViewComponent() {
        return componentDelegate.getViewComponent();
    }

    @Override
    public CC getControllerComponent() {
        return componentDelegate.getControllerComponent();
    }

}
