package view_component.lib_ohos.com.view_component.base_view.layouts;


import ohos.agp.components.AttrSet;
import ohos.agp.components.DependentLayout;
import ohos.app.Context;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import view_component.lib_ohos.com.view_component.base_view.ComponentDelegate;
import view_component.lib_ohos.com.view_component.base_view.ComponentDelegateImpl;
import view_component.lib_ohos.com.view_component.base_view.ControllerComponent;
import view_component.lib_ohos.com.view_component.base_view.ViewComponent;

import static view_component.lib_ohos.com.view_component.base_view.ComponentDelegateImpl.create;

public abstract class ComponentRelativeLayout<VC extends ViewComponent, CC extends ControllerComponent<VC>> extends DependentLayout implements ComponentDelegate<VC, CC> {

    /**
     * componentDelegate
     */
    private final ComponentDelegateImpl<VC, CC> componentDelegate;

    public ComponentRelativeLayout(@NotNull Context context) {
        this(context,null);
    }

    public ComponentRelativeLayout(@NotNull Context context, @Nullable AttrSet attrs) {
        this(context, attrs,null);
    }

    public ComponentRelativeLayout(@NotNull Context context, @Nullable AttrSet attrs, String styleName) {
        super(context, attrs, styleName);
        componentDelegate = create(this, context);
    }

    @Override
    public VC getViewComponent() {
        return componentDelegate.getViewComponent();
    }

    @Override
    public CC getControllerComponent() {
        return componentDelegate.getControllerComponent();
    }

}
