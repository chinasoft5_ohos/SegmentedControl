package view_component.lib_ohos.com.view_component.base_view.functional_interfaces;

import org.jetbrains.annotations.NotNull;

import view_component.lib_ohos.com.view_component.base_view.ViewComponent;

/**
 * Created by Robert Apikyan on 8/18/2017.
 */

public interface Request<VC extends ViewComponent> {

    /**
     * result
     *
     * @param viewComponent viewComponent
     */
    void onResult(@NotNull VC viewComponent);
}
