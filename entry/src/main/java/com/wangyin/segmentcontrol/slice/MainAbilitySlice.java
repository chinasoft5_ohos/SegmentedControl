/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.wangyin.segmentcontrol.slice;

import com.wangyin.segmentcontrol.ResourceTable;

import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;

import ohos.agp.components.Component;

import segmented_control.widget.custom.ohos.com.segmentedcontrol.SegmentedControl;

/**
 * MainAbilitySlice
 *
 * @author wangyin
 * @since 2021/06/23
 */
public class MainAbilitySlice<D> extends AbilitySlice {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);
        Component component = findComponentById(ResourceTable.Id_sc1);
        if (component instanceof SegmentedControl) {
            SegmentedControl component1 = (SegmentedControl) component;
            component1.setSelectedSegment(1, 0);
        }
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }
}
