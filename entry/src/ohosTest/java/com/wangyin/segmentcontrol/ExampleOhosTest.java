/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.wangyin.segmentcontrol;

import org.junit.Test;

import section_layout.widget.custom.ohos.com.sectionlayout.utils.JUtils;

import java.util.logging.Logger;

/**
 * ExampleOhosTest
 *
 * @author wangyin
 * @since 2021/06/23
 */
public class ExampleOhosTest {
    /**
     * 判断字符是否为空
     */
    @Test
    public void jUtils1() {
        boolean isEmpty = JUtils.isEmpty("abc");
        Logger.getLogger("test").info(String.valueOf(isEmpty));
    }

    /**
     * 判断字符是否为空
     */
    @Test
    public void jUtils2() {
        boolean isEmpty = JUtils.isEmpty("");
        Logger.getLogger("test").info(String.valueOf(isEmpty));
    }

}