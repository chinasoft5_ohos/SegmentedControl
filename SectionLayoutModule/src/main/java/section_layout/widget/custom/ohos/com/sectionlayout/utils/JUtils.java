/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package section_layout.widget.custom.ohos.com.sectionlayout.utils;

import ohos.agp.window.service.Display;
import ohos.agp.window.service.DisplayManager;
import ohos.app.Context;

/**
 * JUtils
 *
 * @author wangyin
 * @since 2021/06/23
 */
public class JUtils {
    /**
     * 最后一次点击时间
     */
    private static long sLastClickTime = 0L;

    /**
     * 时间间隔
     */
    private static final long TIME_INTERVAL = 1000L;

    /**
     * 防止重复点击
     *
     * @param callBack 回调事件
     */
    public static void preventRepeatClick(CallBack callBack) {
        long nowTime = System.currentTimeMillis();
        if (nowTime - sLastClickTime > TIME_INTERVAL) {
            // 单次点击事件
            callBack.callBack();
            sLastClickTime = nowTime;
        }
    }

    /**
     * 回调函数
     */
    public interface CallBack {
        /**
         * callBack
         */
        void callBack();
    }

    /**
     * 尺寸转换 vp 2 px
     *
     * @param mContext 上下文
     * @param vp vp值
     * @return px值
     */
    public static float vp2px(Context mContext, float vp) {
        Display display = DisplayManager.getInstance().getDefaultDisplay(mContext).get();
        float dpi = display.getAttributes().densityPixels;
        return (float) (vp * dpi + 0.5 * (vp >= 0 ? 1 : -1));
    }

    /**
     * 判断内容是否为空
     *
     * @param str 内容
     * @return 结果
     */
    public static boolean isEmpty(String str) {
        return str == null || str.length() == 0;
    }
}
