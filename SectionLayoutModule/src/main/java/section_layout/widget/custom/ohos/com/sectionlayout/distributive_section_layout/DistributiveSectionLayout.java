package section_layout.widget.custom.ohos.com.sectionlayout.distributive_section_layout;

import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.app.Context;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import section_layout.widget.custom.ohos.com.sectionlayout.SectionLayout;
import section_layout.widget.custom.ohos.com.sectionlayout.SectionLayoutViewControllerComponent;
import section_layout.widget.custom.ohos.com.sectionlayout.utils.AttrUtils;


/**
 * Subclass of {@link SectionLayout}
 * Supports distribute_evenly attribute.
 * distribute_evenly = true, then childes will be measured with equal width.
 *
 * @param <D> Section Data type
 */
public class DistributiveSectionLayout<D> extends SectionLayout<D> implements Component.EstimateSizeListener {

    public DistributiveSectionLayout(@NotNull Context context) {
        this(context, null);
    }

    public DistributiveSectionLayout(@NotNull Context context, @Nullable AttrSet attrs) {
        this(context, attrs, null);
    }

    public DistributiveSectionLayout(@NotNull Context context, @Nullable AttrSet attrs, String defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initArgs(attrs);
    }

    private void initArgs(@Nullable AttrSet attrs) {
        if (attrs != null) {
            distributeEvenly(AttrUtils.getBooleanFromAttr(attrs, "distribute_evenly", false));
        }


        setEstimateSizeListener(this);
    }

    /**
     * createControllerComponent
     *
     * @return SectionLayoutViewControllerComponent
     */
    @NotNull
    @Override
    public SectionLayoutViewControllerComponent<D> createControllerComponent() {
        return new DistributiveSectionLayoutControllerComponent<>();
    }

    /**
     * distributeEvenly
     *
     * @param willDistributeEvenly willDistributeEvenly
     */
    public void distributeEvenly(boolean willDistributeEvenly) {
        ((DistributiveSectionLayoutControllerComponent<Object>) getControllerComponent()).distributeEvenly(willDistributeEvenly);
    }

    /**
     * onEstimateSize
     *
     * @param widthEstimatedConfig widthEstimatedConfig
     * @param heightEstimatedConfig heightEstimatedConfig
     * @return boolean
     */
    @Override
    public boolean onEstimateSize(int widthEstimatedConfig, int heightEstimatedConfig) {
        // 通知子组件进行测量
        measureChildren(widthEstimatedConfig, heightEstimatedConfig);
        ((DistributiveSectionLayoutControllerComponent<Object>) getControllerComponent()).measureDistribute(widthEstimatedConfig);
        return false;
    }

    private void measureChildren(int widthEstimatedConfig, int heightEstimatedConfig) {
        for (int idx = 0; idx < getChildCount(); idx++) {
            Component childView = getComponentAt(idx);
            if (childView != null) {
                measureChild(childView, widthEstimatedConfig, heightEstimatedConfig);
            }
        }

    }

    private void measureChild(Component child, int parentWidthMeasureSpec, int parentHeightMeasureSpec) {
        //child:stackLayout
        ComponentContainer.LayoutConfig lc = child.getLayoutConfig();
        int childWidthMeasureSpec = EstimateSpec.getChildSizeWithMode(
                lc.width, parentWidthMeasureSpec, EstimateSpec.UNCONSTRAINT);
        int childHeightMeasureSpec = EstimateSpec.getChildSizeWithMode(
                lc.height, parentHeightMeasureSpec, EstimateSpec.UNCONSTRAINT);
        child.estimateSize(childWidthMeasureSpec, childHeightMeasureSpec);
    }
}
