package section_layout.widget.custom.ohos.com.sectionlayout.listeners;


import org.jetbrains.annotations.Nullable;

/**
 * Created by Robert Apikyan on 9/5/2017.
 */

public interface OnAddSectionRequestListener<D> {
    /**
     * Will be called before section addition
     *
     * @param sectionData the section data
     * @param sectionPosition the section position
     * @return true -> the section will be added to section layout: false -> add request will be ignored
     */
    boolean onAddSectionRequest(@Nullable D sectionData, int sectionPosition);
}
