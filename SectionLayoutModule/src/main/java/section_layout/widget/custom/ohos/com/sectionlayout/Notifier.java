package section_layout.widget.custom.ohos.com.sectionlayout;

import section_layout.widget.custom.ohos.com.sectionlayout.listeners.*;

/**
 * Created by Robert Apikyan on 9/5/2017.
 */

class Notifier<D> implements OnAddSectionListener<D>, OnRemoveSectionListener, OnAddSectionRequestListener<D>, OnRemoveSectionRequestListener<D>, OnAllSectionsRemovedListener, OnAllSectionsRemoveRequestListener {
    /**
     * onAddSectionListener
     */
    private OnAddSectionListener<D> onAddSectionListener;

    /**
     * onRemoveSectionListener
     */
    private OnRemoveSectionListener onRemoveSectionListener;

    /**
     * onAddSectionRequestListener
     */
    private OnAddSectionRequestListener<D> onAddSectionRequestListener;

    /**
     * onRemoveSectionRequestListener
     */
    private OnRemoveSectionRequestListener<D> onRemoveSectionRequestListener;

    /**
     * onAllSectionsRemovedListener
     */
    private OnAllSectionsRemovedListener onAllSectionsRemovedListener;

    /**
     * onAllSectionsRemoveRequestListener
     */
    private OnAllSectionsRemoveRequestListener onAllSectionsRemoveRequestListener;

    @Override
    public void onSectionAdded(D sectionData, int sectionPosition) {
        if (onAddSectionListener != null) {
            onAddSectionListener.onSectionAdded(sectionData, sectionPosition);
        }
    }

    @Override
    public void onSectionRemoved(int position) {
        if (onRemoveSectionListener != null) {
            onRemoveSectionListener.onSectionRemoved(position);
        }
    }

    @Override
    public boolean onAddSectionRequest(D sectionData, int sectionPosition) {
        return onAddSectionRequestListener == null || onAddSectionRequestListener.onAddSectionRequest(sectionData, sectionPosition);
    }

    @Override
    public boolean onRemoveSectionRequest(D sectionData) {
        return onRemoveSectionRequestListener == null || onRemoveSectionRequestListener.onRemoveSectionRequest(sectionData);
    }

    @Override
    public void onAllSectionsRemoved() {
        if (onAllSectionsRemovedListener != null) {
            onAllSectionsRemovedListener.onAllSectionsRemoved();
        }
    }

    @Override
    public boolean onAllSectionsRemoveRequest() {
        return onAllSectionsRemoveRequestListener == null || onAllSectionsRemoveRequestListener.onAllSectionsRemoveRequest();
    }

    // setters
    void setOnAddSectionListener(OnAddSectionListener<D> onAddSectionListener) {
        this.onAddSectionListener = onAddSectionListener;
    }

    void setOnRemoveSectionListener(OnRemoveSectionListener onRemoveSectionListener) {
        this.onRemoveSectionListener = onRemoveSectionListener;
    }

    void setOnAddSectionRequestListener(OnAddSectionRequestListener<D> onAddSectionRequestListener) {
        this.onAddSectionRequestListener = onAddSectionRequestListener;
    }

    void setOnRemoveSectionRequestListener(OnRemoveSectionRequestListener<D> onRemoveSectionRequestListener) {
        this.onRemoveSectionRequestListener = onRemoveSectionRequestListener;
    }

    void setOnAllSectionsRemovedListener(OnAllSectionsRemovedListener onAllSectionsRemovedListener) {
        this.onAllSectionsRemovedListener = onAllSectionsRemovedListener;
    }

    void setOnAllSectionsRemoveRequestListener(OnAllSectionsRemoveRequestListener onAllSectionsRemoveRequestListener) {
        this.onAllSectionsRemoveRequestListener = onAllSectionsRemoveRequestListener;
    }

    // getters
    OnAddSectionListener<D> getOnAddSectionListener() {
        return onAddSectionListener;
    }

    OnRemoveSectionListener getOnRemoveSectionListener() {
        return onRemoveSectionListener;
    }

    OnAddSectionRequestListener<D> getOnAddSectionRequestListener() {
        return onAddSectionRequestListener;
    }

    OnRemoveSectionRequestListener<D> getOnRemoveSectionRequestListener() {
        return onRemoveSectionRequestListener;
    }

    OnAllSectionsRemovedListener getOnAllSectionsRemovedListener() {
        return onAllSectionsRemovedListener;
    }

    OnAllSectionsRemoveRequestListener getOnAllSectionsRemoveRequestListener() {
        return onAllSectionsRemoveRequestListener;
    }
}
