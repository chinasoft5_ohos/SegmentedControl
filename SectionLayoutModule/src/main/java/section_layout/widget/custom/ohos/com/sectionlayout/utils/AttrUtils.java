/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package section_layout.widget.custom.ohos.com.sectionlayout.utils;

import ohos.agp.components.AttrSet;
import ohos.agp.components.element.Element;

/**
 * 自定义属性工具类
 */
public final class AttrUtils {
    private AttrUtils() {
    }

    /**
     * 获取尺寸属性
     *
     * @param attrSet 属性集合
     * @param name 属性名称
     * @param defaultValue 默认值
     * @return 获取的值
     */
    public static String getStringFromAttr(AttrSet attrSet, String name, String defaultValue) {
        String value = defaultValue;
            if (attrSet.getAttr(name) != null && attrSet.getAttr(name).isPresent()) {
                value = attrSet.getAttr(name).get().getStringValue();
            }
        return value;
    }

    /**
     * 获取尺寸属性
     *
     * @param attrSet 属性集合
     * @param name 属性名称
     * @param defaultValue 默认值
     * @return 获取的值
     */
    public static Integer getIntegerFromAttr(AttrSet attrSet, String name, Integer defaultValue) {
        Integer value = defaultValue;
            if (attrSet.getAttr(name) != null && attrSet.getAttr(name).isPresent()) {
                value = attrSet.getAttr(name).get().getIntegerValue();
            }
        return value;
    }

    /**
     * 获取尺寸属性
     *
     * @param attrSet 属性集合
     * @param name 属性名称
     * @param defaultValue 默认值
     * @return 获取的值
     */
    public static float getFloatFromAttr(AttrSet attrSet, String name, float defaultValue) {
        float value = defaultValue;
            if (attrSet!=null&&attrSet.getAttr(name) != null && attrSet.getAttr(name).isPresent()) {
                value = attrSet.getAttr(name).get().getFloatValue();
            }
        return value;
    }

    /**
     * 获取尺寸属性
     *
     * @param attrSet 属性集合
     * @param name 属性名称
     * @param defaultValue 默认值
     * @return 获取的值
     */
    public static Long getLongFromAttr(AttrSet attrSet, String name, Long defaultValue) {
        Long value = defaultValue;
            if (attrSet.getAttr(name) != null && attrSet.getAttr(name).isPresent()) {
                value = attrSet.getAttr(name).get().getLongValue();
            }
        return value;
    }

    /**
     * 获取尺寸属性
     *
     * @param attrSet 属性集合
     * @param name 属性名称
     * @param defaultElement 默认值
     * @return 获取的值
     */
    public static Element getElementFromAttr(AttrSet attrSet, String name, Element defaultElement) {
        Element element = defaultElement;
            if (attrSet.getAttr(name) != null && attrSet.getAttr(name).isPresent()) {
                element = attrSet.getAttr(name).get().getElement();
            }
        return element;
    }


    /**
     * 获取尺寸属性
     *
     * @param attrSet 属性集合
     * @param name 属性名称
     * @param defaultValue 默认值
     * @return 获取的值
     */
    public static Integer getDimensionFromAttr(AttrSet attrSet, String name, Integer defaultValue) {
        Integer value = defaultValue;
            if (attrSet.getAttr(name) != null && attrSet.getAttr(name).isPresent()) {
                value = attrSet.getAttr(name).get().getDimensionValue();
            }
        return value;
    }

    /**
     * 获取颜色属性
     *
     * @param attrSet 属性集合
     * @param name 属性名称
     * @param defaultValue 默认值
     * @return 获取的值
     */
    public static int getColorFromAttr(AttrSet attrSet, String name, int defaultValue) {
        int value = defaultValue;
            if (attrSet.getAttr(name) != null && attrSet.getAttr(name).isPresent()) {
                value = attrSet.getAttr(name).get().getColorValue().getValue();
            }
        return value;
    }

    /**
     * 获取boolean
     *
     * @param attrSet 属性集合
     * @param name 属性名称
     * @param defaultValue 默认值
     * @return 获取的值
     */
    public static boolean getBooleanFromAttr(AttrSet attrSet, String name, boolean defaultValue) {
        boolean value = defaultValue;
            if (attrSet.getAttr(name) != null && attrSet.getAttr(name).isPresent()) {
                value = attrSet.getAttr(name).get().getBoolValue();
            }
        return value;
    }
}
