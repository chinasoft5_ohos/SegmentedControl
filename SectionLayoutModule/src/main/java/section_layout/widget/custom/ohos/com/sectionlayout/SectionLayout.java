package section_layout.widget.custom.ohos.com.sectionlayout;


import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.LayoutScatter;
import ohos.app.Context;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import section_layout.widget.custom.ohos.com.sectionlayout.listeners.*;

import view_component.lib_ohos.com.view_component.base_view.layouts.ComponentLinearLayout;

/**
 * Basic usage
 * sectionLayout data type is defined as String;
 * <p>
 * sectionLayout.withAdapter(new ItemAdapter())
 * .addSection("section one")
 * .addSection("section two");
 *
 * @param <D> Section Data type
 */
public class SectionLayout<D> extends ComponentLinearLayout<SectionLayoutViewComponent, SectionLayoutViewControllerComponent<D>> {
    public SectionLayout(@NotNull Context context) {
        super(context);
    }

    public SectionLayout(@NotNull Context context, @Nullable AttrSet attrs) {
        super(context, attrs);
    }

    public SectionLayout(@NotNull Context context, @Nullable AttrSet attrs,String defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    /**
     * createViewComponent
     *
     * @param layoutInflater layoutInflater
     * @return SectionLayoutViewComponent
     */
    @NotNull
    @Override
    public SectionLayoutViewComponent createViewComponent(@NotNull LayoutScatter layoutInflater) {
        return new SectionLayoutViewComponent(this);
    }

    /**
     * createViewComponent
     *
     * @return SectionLayoutViewComponent
     */
    @NotNull
    @Override
    public SectionLayoutViewControllerComponent<D> createControllerComponent() {
        return new SectionLayoutViewControllerComponent<>();
    }

    /**
     * withAdapter
     *
     * @param adapter adapter
     * @return SectionManager
     */
    public SectionManager<D> withAdapter(@NotNull Adapter<D,ViewHolder<D>> adapter) {
        return getControllerComponent().withAdapter(adapter);
    }

    /**
     * addSection
     *
     * @param sectionData sectionData
     * @return SectionManager
     */
    public SectionManager<D> addSection(D sectionData) {
        return getControllerComponent().addSection(sectionData);
    }

    /**
     * addNullableSection
     *
     * @return SectionManager
     */
    public SectionManager<D> addNullableSection() {
        return getControllerComponent().addNullableSection();
    }

    /**
     * insertSectionAtPosition
     *
     * @param sectionData sectionData
     * @param sectionPosition sectionPosition
     * @return SectionManager
     */
    public SectionManager<D> insertSectionAtPosition(D sectionData, int sectionPosition) {
        return getControllerComponent().insertSectionAtPosition(sectionData, sectionPosition);
    }

    /**
     * removeFirstSectionWithData
     *
     * @param sectionData sectionData
     * @return SectionManager
     */
    public SectionManager<D> removeFirstSectionWithData(D sectionData) {
        return getControllerComponent().removeFirstSectionWithData(sectionData);
    }

    /**
     * removeAllSectionsWithData
     *
     * @param sectionData sectionData
     * @return SectionManager
     */
    public SectionManager<D> removeAllSectionsWithData(D sectionData) {
        return getControllerComponent().removeAllSectionsWithData(sectionData);
    }

    /**
     * removeLastSection
     *
     * @return SectionManager
     */
    public SectionManager<D> removeLastSection() {
        return getControllerComponent().removeLastSection();
    }

    /**
     * removeFirstSection
     *
     * @return SectionManager
     */
    public SectionManager<D> removeFirstSection() {
        return getControllerComponent().removeFirstSection();
    }

    /**
     * removeSectionAtPosition
     *
     * @param sectionPosition sectionPosition
     * @return SectionManager
     */
    public SectionManager<D> removeSectionAtPosition(int sectionPosition) {
        return getControllerComponent().removeSectionAtPosition(sectionPosition);
    }

    /**
     * removeAllSections
     *
     * @return SectionManager
     */
    public SectionManager<D> removeAllSections() {
        return getControllerComponent().removeAllSections();
    }

    /**
     * getViewHolderForAdapterPosition
     *
     * @param adapterPosition adapterPosition
     * @return ViewHolder
     */
    public ViewHolder<D> getViewHolderForAdapterPosition(int adapterPosition) {
        return getControllerComponent().getViewHolderForAdapterPosition(adapterPosition);
    }

    /**
     * getFirstViewHolderWithData
     *
     * @param sectionData sectionData
     * @return ViewHolder
     */
    @Nullable
    public ViewHolder<D> getFirstViewHolderWithData(D sectionData) {
        return getControllerComponent().getFirstViewHolderWithData(sectionData);
    }

    /**
     * Use this method to fetch current items count
     *
     * @return items count
     */
    public int size() {
        return getControllerComponent().size();
    }

    /**
     * setOnAddSectionListener
     *
     * @param onAddSectionListener onAddSectionListener
     */
    public void setOnAddSectionListener(OnAddSectionListener<D> onAddSectionListener) {
        getControllerComponent().setOnAddSectionListener(onAddSectionListener);
    }

    /**
     * setOnRemoveSectionListener
     *
     * @param onRemoveSectionListener onRemoveSectionListener
     */
    public void setOnRemoveSectionListener(OnRemoveSectionListener onRemoveSectionListener) {
        getControllerComponent().setOnRemoveSectionListener(onRemoveSectionListener);
    }

    /**
     * setOnAddSectionRequestListener
     *
     * @param onAddSectionRequestListener onAddSectionRequestListener
     */
    public void setOnAddSectionRequestListener(OnAddSectionRequestListener<D> onAddSectionRequestListener) {
        getControllerComponent().setOnAddSectionRequestListener(onAddSectionRequestListener);
    }

    /**
     * setOnRemoveSectionRequestListener
     *
     * @param onRemoveSectionRequestListener onRemoveSectionRequestListener
     */
    public void setOnRemoveSectionRequestListener(OnRemoveSectionRequestListener<D> onRemoveSectionRequestListener) {
        getControllerComponent().setOnRemoveSectionRequestListener(onRemoveSectionRequestListener);
    }

    /**
     * setOnAllSectionsRemovedListener
     *
     * @param onAllSectionsRemovedListener onAllSectionsRemovedListener
     */
    public void setOnAllSectionsRemovedListener(OnAllSectionsRemovedListener onAllSectionsRemovedListener) {
        getControllerComponent().setOnAllSectionsRemovedListener(onAllSectionsRemovedListener);
    }

    /**
     * setOnAllSectionsRemoveRequestListener
     *
     * @param onAllSectionsRemoveRequestListener onAllSectionsRemoveRequestListener
     */
    public void setOnAllSectionsRemoveRequestListener(OnAllSectionsRemoveRequestListener onAllSectionsRemoveRequestListener) {
        getControllerComponent().setOnAllSectionsRemoveRequestListener(onAllSectionsRemoveRequestListener);
    }

    /**
     * getOnAddSectionListener
     *
     * @return OnAddSectionListener
     */
    public OnAddSectionListener<D> getOnAddSectionListener() {
        return getControllerComponent().getOnAddSectionListener();
    }

    /**
     * getOnRemoveSectionListener
     *
     * @return OnRemoveSectionListener
     */
    public OnRemoveSectionListener getOnRemoveSectionListener() {
        return getControllerComponent().getOnRemoveSectionListener();
    }

    /**
     * getOnAddSectionRequestListener
     *
     * @return OnAddSectionRequestListener
     */
    public OnAddSectionRequestListener<D> getOnAddSectionRequestListener() {
        return getControllerComponent().getOnAddSectionRequestListener();
    }

    /**
     * getOnRemoveSectionRequestListener
     *
     * @return OnRemoveSectionRequestListener
     */
    public OnRemoveSectionRequestListener<D> getOnRemoveSectionRequestListener() {
        return getControllerComponent().getOnRemoveSectionRequestListener();
    }

    /**
     * getOnAllSectionsRemovedListener
     *
     * @return OnAllSectionsRemovedListener
     */
    public OnAllSectionsRemovedListener getOnAllSectionsRemovedListener() {
        return getControllerComponent().getOnAllSectionsRemovedListener();
    }

    /**
     * getOnAllSectionsRemoveRequestListener
     *
     * @return OnAllSectionsRemoveRequestListener
     */
    public OnAllSectionsRemoveRequestListener getOnAllSectionsRemoveRequestListener() {
        return getControllerComponent().getOnAllSectionsRemoveRequestListener();
    }

    /**
     * ViewHolder
     *
     * @param <D>
     */
    public abstract static class ViewHolder<D> {
        /**
         * sectionPosition
         */
        private int sectionPosition;
        /**
         * sectionView
         */
        private final Component sectionView;
        /**
         * sectionData
         */
        private D sectionData;

        public ViewHolder(@NotNull Component sectionView) {
            this.sectionView = sectionView;
        }

        /**
         * This method will be called inside {@link Adapter#onBindViewHolder(ViewHolder, Object, int)} method,
         * in order to pass view holders data
         *
         * @param sectionData Nullable section data
         */
        final void bind(@Nullable D sectionData) {
            this.sectionData = sectionData;
            onBind(sectionData);
        }

        /**
         * getSectionPosition
         *
         * @return section position
         */
        public int getSectionPosition() {
            return sectionPosition;
        }

        /**
         * getSectionView
         *
         * @return Section root view
         */
        public Component getSectionView() {
            return sectionView;
        }

        /**
         * getSectionData
         *
         * @return Nullable section data,
         */
        @Nullable
        public D getSectionData() {
            return sectionData;
        }

        void setSectionPosition(int sectionPosition) {
            this.sectionPosition = sectionPosition;
        }

        /**
         * This method will be called inside view holders {@link #bind(Object)} method, after setting sectionData
         *
         * @param sectionData Section view holders data
         */
        protected abstract void onBind(D sectionData);
    }

    /**
     * Make subclass of {@link Adapter} and pass inside {@link SectionLayout#withAdapter(Adapter)} method
     *
     * @param <D> Section item data type
     */
    public abstract static class Adapter<D, VH extends ViewHolder<D>> {
        /**
         * DEFAULT_VIEW_TYPE
         */
        private static final int DEFAULT_VIEW_TYPE = 0xfffe8a7b;

        /**
         *Method will be called inside {@link SectionManager#insertSectionAtPosition(Object, int)} method in order
         *          * to create {@link NotNull} view holder;
         *
         * @param layoutInflater NonNull LayoutInflater
         * @param context context
         * @param parent parent
         * @param viewType Item view type, Override method {@link Adapter#getViewType(Object, int)} and return view type depending on sectionData or sectionPosition. Default value is {@link Adapter#DEFAULT_VIEW_TYPE}
         * @return NonNull ViewHolder
         */
        @NotNull
        protected abstract VH onCreateViewHolder(@NotNull LayoutScatter layoutInflater,Context context, ComponentContainer parent, int viewType);

        /**
         * Method will be called inside {@link SectionManager#insertSectionAtPosition(Object, int)} method,
         * in order to pass section data inside NonNull viewHolder
         *
         * @param viewHolder NonNull viewHolder
         * @param sectionData Nullable sectionData, {@link SectionManager#addNullableSection()}
         * @param sectionPosition the section position;
         */
        protected void onBindViewHolder(@NotNull VH viewHolder, @Nullable D sectionData, int sectionPosition) {
            viewHolder.bind(sectionData);
        }

        /**
         * Override this method and return your View type depending on sectionData and sectionPosition values
         *
         * @param sectionData Nullable section data
         * @param sectionPosition the section position
         * @return view type. Default value is {@link Adapter#DEFAULT_VIEW_TYPE}
         */
        protected int getViewType(@Nullable D sectionData, int sectionPosition) {
            return DEFAULT_VIEW_TYPE;
        }
    }
}
