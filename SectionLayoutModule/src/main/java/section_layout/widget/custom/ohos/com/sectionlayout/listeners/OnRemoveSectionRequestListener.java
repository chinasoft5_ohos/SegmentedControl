package section_layout.widget.custom.ohos.com.sectionlayout.listeners;

import org.jetbrains.annotations.Nullable;

/**
 * Created by Robert Apikyan on 9/5/2017.
 */

public interface OnRemoveSectionRequestListener<D> {
    /**
     * Will be called before section remove action performing
     *
     * @param sectionData the removed section data
     * @return boolean
     */
    boolean onRemoveSectionRequest(@Nullable D sectionData);
}
