package section_layout.widget.custom.ohos.com.sectionlayout.listeners;

/**
 * OnAllSectionsRemoveRequestListener
 *
 * @since 2021/06/23
 */
public interface OnAllSectionsRemoveRequestListener {
    /**
     * Will be called before all section remove action
     *
     * @return true -> all sections will be removed: false -> remove all sections request will be ignored
     */
    boolean onAllSectionsRemoveRequest();
}
