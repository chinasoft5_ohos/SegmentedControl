package section_layout.widget.custom.ohos.com.sectionlayout;

import ohos.agp.components.Component;

import org.jetbrains.annotations.NotNull;

import view_component.lib_ohos.com.view_component.base_view.ViewComponent;

/**
 * Created by Robert Apikyan on 8/22/2017.
 */

public class SectionLayoutViewComponent extends ViewComponent {
    public SectionLayoutViewComponent(@NotNull Component rootView) {
        super(rootView);
    }
}
