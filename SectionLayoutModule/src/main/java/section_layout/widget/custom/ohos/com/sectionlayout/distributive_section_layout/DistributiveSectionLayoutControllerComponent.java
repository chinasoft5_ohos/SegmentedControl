package section_layout.widget.custom.ohos.com.sectionlayout.distributive_section_layout;


import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.DirectionalLayout;
import ohos.agp.components.StackLayout;

import section_layout.widget.custom.ohos.com.sectionlayout.SectionLayoutViewComponent;
import section_layout.widget.custom.ohos.com.sectionlayout.SectionLayoutViewControllerComponent;

/**
 * Created by Robert Apikyan on 9/5/2017.
 */

public class DistributiveSectionLayoutControllerComponent<D> extends SectionLayoutViewControllerComponent<D> {
    /**
     * willDistributeEvenly
     */
    private boolean willDistributeEvenly;

    void distributeEvenly(boolean willDistributeEvenly) {
        this.willDistributeEvenly = willDistributeEvenly;
    }

    void measureDistribute(int widthMeasureSpec) {
        SectionLayoutViewComponent viewComponent = getViewComponent();
        //noinspection ConstantConditions, measureDistributeEvenly
        if (viewComponent == null) {
            return;
        }
        ComponentContainer parent = viewComponent.getRootViewGroup();
        if (parent == null) {
            return;
        }
        //DistributiveSectionLayout
        if (parent.getChildCount() > 0) {
            int maxWidth = 0;

            if (isWidthSetToMatchParent(parent)) {
                // MATCH PARENT
                if (willDistributeEvenly) {
                    //maxWidth=519
                    maxWidth = Component.EstimateSpec.getSize(widthMeasureSpec) / parent.getChildCount();
                    setChildesWidth(maxWidth, parent);
                } else {
                    //MATCH PARENT and distributeEvenly = false
                    double maxLength = 0;
                    for (int i = 0; i < parent.getChildCount(); i++) {
                        Component child = parent.getComponentAt(i);
                        maxLength += child.getEstimatedWidth();
                    }
                    //因为精度问题,宽度会有差别,需要修复
                    double realWidth = 0;
                    int totalWidth = Component.EstimateSpec.getSize(widthMeasureSpec);
                    for (int i = 0; i < parent.getChildCount(); i++) {
                        StackLayout stackLayout = (StackLayout) parent.getComponentAt(i);
                        int percentageWidth = (int) (stackLayout.getEstimatedWidth() / maxLength * totalWidth);
                        realWidth += percentageWidth;
                        if (i == parent.getChildCount() - 1 && realWidth < totalWidth) {
                            //总宽度修复
                            percentageWidth += totalWidth - realWidth;
                        }
                        stackLayout.setWidth(percentageWidth);
                    }
                }
            } else if (isWidthSetToWrapContent(parent)) {
                //WRAP CONTENT and distributeEvenly = true
                if (willDistributeEvenly) {
                    for (int i = 0; i < parent.getChildCount(); i++) {
                        int nextChildWidth = parent.getComponentAt(i).getEstimatedWidth();
                        maxWidth = Math.max(maxWidth, nextChildWidth);
                        setChildesWidth(maxWidth, parent);
                    }
                }// if wrap content and distributeEvenly = false, no need to measure
            } else {
                // if parent has fixed size and distributeEvenly = true
                if (willDistributeEvenly) {
                    maxWidth = parent.getLayoutConfig().width / parent.getChildCount();
                    setChildesWidth(maxWidth, parent);
                }
            }
        }
    }

    private void setChildesWidth(int maxWidth, ComponentContainer parent) {
        for (int i = 0; i < parent.getChildCount(); i++) {
            parent.getComponentAt(i).setWidth(maxWidth);
        }
    }

    private boolean isWidthSetToMatchParent(ComponentContainer parent) {//parent:directionalSectionLayout
        return DirectionalLayout.LayoutConfig.MATCH_PARENT == parent.getLayoutConfig().width;
    }

    private boolean isWidthSetToWrapContent(ComponentContainer parent) {
        return DirectionalLayout.LayoutConfig.MATCH_CONTENT == parent.getLayoutConfig().width;
    }
}
