package section_layout.widget.custom.ohos.com.sectionlayout.listeners;

/**
 * OnRemoveSectionListener
 *
 * @since 2021/06/23
 */
public interface OnRemoveSectionListener {
    /**
     * Will be called when section is removed
     *
     * @param sectionPosition the removed section position
     */
    void onSectionRemoved(int sectionPosition);
}
