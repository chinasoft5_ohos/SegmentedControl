package section_layout.widget.custom.ohos.com.sectionlayout.listeners;


import section_layout.widget.custom.ohos.com.sectionlayout.SectionManager;

/**
 * Only for {@link SectionManager#removeAllSections()} method
 */
public interface OnAllSectionsRemovedListener {
    /**
     * Will be called after all sections removed
     */
    void onAllSectionsRemoved();
}
