package segmented_control.widget.custom.ohos.com.segmentedcontrol;

/**
 * Consumer
 *
 * @author wangyin
 * @since 2021/07/02
 */
interface Consumer<T> {

    /**
     * apply
     *
     * @param t t
     */
    void apply(T t);
}
