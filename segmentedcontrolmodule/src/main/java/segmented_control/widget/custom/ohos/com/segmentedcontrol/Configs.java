package segmented_control.widget.custom.ohos.com.segmentedcontrol;


import segmented_control.widget.custom.ohos.com.segmentedcontrol.item_row_column.SegmentDecoration;

/**
 * Created by Robert Apikyan on 9/7/2017.
 */

class Configs {
    /**
     * DEFAULT_COLUMN_COUNT
     */
    static final int DEFAULT_COLUMN_COUNT = 2;
    /**
     * DEFAULT_SUPPORTED_SELECTIONS_COUNT
     */
    static final int DEFAULT_SUPPORTED_SELECTIONS_COUNT = 1;
    /**
     * willDistributeEvenly
     */
    boolean willDistributeEvenly;
    /**
     * reselectionEnabled
     */
    boolean reselectionEnabled;
    /**
     * columnCount
     */
    int columnCount;
    /**
     * supportedSelectionsCount
     */
    int supportedSelectionsCount;

    SegmentDecoration segmentDecoration = new SegmentDecoration();

    static Configs getDefault() {
        Configs configs = new Configs();
        configs.reselectionEnabled = true;
        configs.willDistributeEvenly = false;
        configs.columnCount = DEFAULT_COLUMN_COUNT;
        configs.supportedSelectionsCount = DEFAULT_SUPPORTED_SELECTIONS_COUNT;
        return configs;
    }
}
