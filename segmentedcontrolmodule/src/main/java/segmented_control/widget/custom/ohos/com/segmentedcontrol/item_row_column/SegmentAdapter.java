package segmented_control.widget.custom.ohos.com.segmentedcontrol.item_row_column;


import section_layout.widget.custom.ohos.com.sectionlayout.distributive_section_layout.DistributiveSectionLayout;

/**
 * Created by Robert Apikyan on 9/7/2017.
 */

public abstract class SegmentAdapter<D, VH extends SegmentViewHolder<D>> extends DistributiveSectionLayout.Adapter<SegmentData<D>, VH> {
}
