package segmented_control.widget.custom.ohos.com.segmentedcontrol.custom_segment;


import ohos.agp.components.ComponentContainer;
import ohos.agp.components.LayoutScatter;
import ohos.app.Context;

import org.jetbrains.annotations.NotNull;

import segmented_control.widget.custom.ohos.com.segmentedcontrol.ResourceTable;
import segmented_control.widget.custom.ohos.com.segmentedcontrol.item_row_column.SegmentAdapter;

/**
 * Created by Robert Apikyan on 9/8/2017.
 */

public class SegmentAdapterImpl extends SegmentAdapter<CharSequence, SegmentViewHolderImpl> {
    @NotNull
    @Override
    protected SegmentViewHolderImpl onCreateViewHolder(@NotNull LayoutScatter layoutInflater, Context context, ComponentContainer viewGroup, int i) {
        return new SegmentViewHolderImpl(layoutInflater.parse(ResourceTable.Layout_item_segment_impl, null,false));
    }
}
