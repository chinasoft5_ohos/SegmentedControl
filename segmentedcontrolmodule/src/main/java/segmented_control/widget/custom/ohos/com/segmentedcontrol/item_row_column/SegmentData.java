package segmented_control.widget.custom.ohos.com.segmentedcontrol.item_row_column;


import segmented_control.widget.custom.ohos.com.segmentedcontrol.listeners.OnSegmentClickListener;

/**
 * Created by Robert Apikyan on 9/7/2017.
 */

public class SegmentData<D> {
    /**
     * isSelected
     */
    boolean isSelected;
    /**
     * absolutePosition
     */
    int absolutePosition;
    /**
     * row
     */
    private int row;
    /**
     * column
     */
    private int column;
    /**
     * segmentData
     */
    private D segmentData;
    /**
     * onSegmentClickListener
     */
    private OnSegmentClickListener<D> onSegmentClickListener;
    /**
     * segmentDecoration
     */
    private SegmentDecoration segmentDecoration;
    /**
     * currentSize
     */
    private int currentSize;
    /**
     * columnCount
     */
    private int columnCount;

    /**
     * SegmentData
     *
     * @param segmentData segmentData
     * @param onSegmentClickListener onSegmentClickListener
     * @param absolutePosition absolutePosition
     * @param row row
     * @param column column
     * @param currentSize currentSize
     * @param columnCount columnCount
     * @param segmentDecoration segmentDecoration
     * @param <D> d
     * @return SegmentData
     */
    public static <D> SegmentData<D> create(D segmentData, OnSegmentClickListener<D> onSegmentClickListener, int absolutePosition, int row, int column, int currentSize, int columnCount, SegmentDecoration segmentDecoration) {
        SegmentData<D> sd = new SegmentData<>();
        sd.segmentData = segmentData;
        sd.absolutePosition = absolutePosition;
        sd.row = row;
        sd.column = column;
        sd.currentSize = currentSize;
        sd.columnCount = columnCount;
        sd.segmentDecoration = segmentDecoration;
        sd.onSegmentClickListener = onSegmentClickListener;
        return sd;
    }

    /**
     * isSelected
     *
     * @return boolean
     */
    public boolean isSelected() {
        return isSelected;
    }

    /**
     * getAbsolutePosition
     *
     * @return int
     */
    public int getAbsolutePosition() {
        return absolutePosition;
    }

    /**
     * getRow
     *
     * @return int
     */
    public int getRow() {
        return row;
    }

    /**
     * getColumn
     *
     * @return int
     */
    public int getColumn() {
        return column;
    }

    /**
     * getSegmentData
     *
     * @return D
     */
    public D getSegmentData() {
        return segmentData;
    }

    /**
     * getOnSegmentClickListener
     *
     * @return OnSegmentClickListener
     */
    public OnSegmentClickListener<D> getOnSegmentClickListener() {
        return onSegmentClickListener;
    }

    /**
     * getSelectedStrokeColor
     *
     * @return int
     */
    public int getSelectedStrokeColor() {
        return segmentDecoration.getSelectedStrokeColor();
    }

    /**
     * getUnSelectedStrokeColor
     *
     * @return int
     */
    public int getUnSelectedStrokeColor() {
        return segmentDecoration.getUnSelectedStrokeColor();
    }

    /**
     * getStrokeWidth
     *
     * @return int
     */
    public int getStrokeWidth() {
        return segmentDecoration.getStrokeWidth();
    }

    /**
     * getSelectBackgroundColor
     *
     * @return int
     */
    public int getSelectBackgroundColor() {
        return segmentDecoration.getSelectBackgroundColor();
    }

    /**
     * getUnSelectedBackgroundColor
     *
     * @return int
     */
    public int getUnSelectedBackgroundColor() {
        return segmentDecoration.getUnSelectedBackgroundColor();
    }

    /**
     * getFocusedBackgroundColor
     *
     * @return int
     */
    public int getFocusedBackgroundColor(){
        return segmentDecoration.getFocusedBackgroundColor();
    }

    /**
     * getSelectionAnimationDuration
     *
     * @return int
     */
    public int getSelectionAnimationDuration(){
        return segmentDecoration.getSelectionAnimationDuration();
    }

    /**
     * getSelectedTextColor
     *
     * @return int
     */
    public int getSelectedTextColor() {
        return segmentDecoration.getSelectedTextColor();
    }

    /**
     * getUnSelectedTextColor
     *
     * @return int
     */
    public int getUnSelectedTextColor() {
        return segmentDecoration.getUnSelectedTextColor();
    }

    /**
     * getTextSize
     *
     * @return int
     */
    public int getTextSize() {
        return segmentDecoration.getTextSize();
    }

    /**
     * getTextHorizontalPadding
     *
     * @return int
     */
    public int getTextHorizontalPadding() {
        return segmentDecoration.getTextHorizontalPadding();
    }

    /**
     * getTextVerticalPadding
     *
     * @return int
     */
    public int getTextVerticalPadding() {
        return segmentDecoration.getTextVerticalPadding();
    }

    /**
     * getSegmentVerticalMargin
     *
     * @return int
     */
    public int getSegmentVerticalMargin() {
        return segmentDecoration.getSegmentVerticalMargin();
    }

    /**
     * getSegmentHorizontalMargin
     *
     * @return int
     */
    public int getSegmentHorizontalMargin() {
        return segmentDecoration.getSegmentHorizontalMargin();
    }

    /**
     * getTopLeftRadius
     *
     * @return int
     */
    public int getTopLeftRadius() {
        return segmentDecoration.getTopLeftRadius();
    }

    /**
     * getTopRightRadius
     *
     * @return int
     */
    public int getTopRightRadius() {
        return segmentDecoration.getTopRightRadius();
    }

    /**
     * getBottomRightRadius
     *
     * @return int
     */
    public int getBottomRightRadius() {
        return segmentDecoration.getBottomRightRadius();
    }

    /**
     * getBottomLeftRadius
     *
     * @return int
     */
    public int getBottomLeftRadius() {
        return segmentDecoration.getBottomLeftRadius();
    }

    /**
     * isRadiusForEverySegment
     *
     * @return boolean
     */
    public boolean isRadiusForEverySegment() {
        return segmentDecoration.isRadiusForEverySegment();
    }

    /**
     * getCurrentSize
     *
     * @return int
     */
    public int getCurrentSize() {
        return currentSize;
    }

    /**
     * getColumnCount
     *
     * @return int
     */
    public int getColumnCount() {
        return columnCount;
    }
}
