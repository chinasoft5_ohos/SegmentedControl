package segmented_control.widget.custom.ohos.com.segmentedcontrol.item_row_column;


import ohos.agp.utils.Color;
import ohos.app.Context;

/**
 * Created by Robert Apikyan on 9/11/2017.
 */

public class SegmentDecoration {
    /**
     * DEFAULT_FOCUSED_BACKGROUND_ALPHA
     */
    public static final int DEFAULT_FOCUSED_BACKGROUND_ALPHA = 36;
    /**
     * DEFAULT_SELECTION_ANIMATION_DURATION
     */
    public static final int DEFAULT_SELECTION_ANIMATION_DURATION = 196;

    // segment decoration
    // stroke
    /**
     * selectedStrokeColor
     */
    public int selectedStrokeColor;
    /**
     * unSelectedStrokeColor
     */
    public int unSelectedStrokeColor;
    /**
     * strokeWidth
     */
    public int strokeWidth;
    // background
    /**
     * selectBackgroundColor
     */
    public int selectBackgroundColor;
    /**
     * unSelectedBackgroundColor
     */
    public int unSelectedBackgroundColor;
    // focused segment background
    /**
     * focusedBackgroundColor
     */
    public int focusedBackgroundColor;
    // selection Animation Duration
    /**
     * selectionAnimationDuration
     */
    public int selectionAnimationDuration;
    // text
    /**
     * selectedTextColor
     */
    public int selectedTextColor;
    /**
     * unSelectedTextColor
     */
    public int unSelectedTextColor;
    /**
     * textSize
     */
    public int textSize;
    // text padding
    /**
     * textHorizontalPadding
     */
    public int textHorizontalPadding;
    /**
     * textVerticalPadding
     */
    public int textVerticalPadding;

    // segment margins
    /**
     * segmentVerticalMargin
     */
    public int segmentVerticalMargin;
    /**
     * segmentHorizontalMargin
     */
    public int segmentHorizontalMargin;

    // segment font type

    // radius
    /**
     * topLeftRadius
     */
    public int topLeftRadius;
    /**
     * topRightRadius
     */
    public int topRightRadius;
    /**
     * bottomRightRadius
     */
    public int bottomRightRadius;
    /**
     * bottomLeftRadius
     */
    public int bottomLeftRadius;
    /**
     * radiusForEverySegment
     */
    public boolean radiusForEverySegment = false; // def. value

    /**
     * createDefault
     *
     * @param context context
     * @param accentColor accentColor
     * @return SegmentDecoration
     */
    public static SegmentDecoration createDefault(Context context, int accentColor) {
        SegmentDecoration sd = new SegmentDecoration();
        sd.selectedStrokeColor = accentColor;
        sd.unSelectedStrokeColor = accentColor;
        sd.selectBackgroundColor = accentColor;
        sd.unSelectedBackgroundColor = Color.TRANSPARENT.getValue();
        sd.focusedBackgroundColor = Color.argb(DEFAULT_FOCUSED_BACKGROUND_ALPHA,
                (accentColor >> 16) & 0xFF,
                (accentColor >> 8) & 0xFF,
                accentColor & 0xFF);
        sd.selectionAnimationDuration = DEFAULT_SELECTION_ANIMATION_DURATION;
        sd.selectedTextColor = Color.WHITE.getValue();
        sd.unSelectedTextColor = accentColor;
        sd.strokeWidth = 1;
        return sd;
    }

    /**
     * getSelectedStrokeColor
     *
     * @return int
     */
    public int getSelectedStrokeColor() {
        return selectedStrokeColor;
    }

    /**
     * getUnSelectedStrokeColor
     *
     * @return int
     */
    public int getUnSelectedStrokeColor() {
        return unSelectedStrokeColor;
    }

    /**
     * getStrokeWidth
     *
     * @return int
     */
    public int getStrokeWidth() {
        return strokeWidth;
    }

    /**
     * getSelectBackgroundColor
     *
     * @return int
     */
    public int getSelectBackgroundColor() {
        return selectBackgroundColor;
    }

    /**
     * getUnSelectedBackgroundColor
     *
     * @return int
     */
    public int getUnSelectedBackgroundColor() {
        return unSelectedBackgroundColor;
    }

    /**
     * getFocusedBackgroundColor
     *
     * @return int
     */
    public int getFocusedBackgroundColor() {
        return focusedBackgroundColor;
    }

    /**
     * getSelectionAnimationDuration
     *
     * @return int
     */
    public int getSelectionAnimationDuration() {
        return selectionAnimationDuration;
    }

    /**
     * getSelectedTextColor
     *
     * @return int
     */
    public int getSelectedTextColor() {
        return selectedTextColor;
    }

    /**
     * getUnSelectedTextColor
     *
     * @return int
     */
    public int getUnSelectedTextColor() {
        return unSelectedTextColor;
    }

    /**
     * getTextSize
     *
     * @return int
     */
    public int getTextSize() {
        return textSize;
    }

    /**
     * getTextHorizontalPadding
     *
     * @return int
     */
    public int getTextHorizontalPadding() {
        return textHorizontalPadding;
    }

    /**
     * getTextVerticalPadding
     *
     * @return int
     */
    public int getTextVerticalPadding() {
        return textVerticalPadding;
    }

    /**
     * getSegmentVerticalMargin
     *
     * @return int
     */
    public int getSegmentVerticalMargin() {
        return segmentVerticalMargin;
    }

    /**
     * getSegmentHorizontalMargin
     *
     * @return int
     */
    public int getSegmentHorizontalMargin() {
        return segmentHorizontalMargin;
    }

    /**
     * getTopLeftRadius
     *
     * @return int
     */
    public int getTopLeftRadius() {
        return topLeftRadius;
    }

    /**
     * getTopRightRadius
     *
     * @return int
     */
    public int getTopRightRadius() {
        return topRightRadius;
    }

    /**
     * getBottomRightRadius
     *
     * @return int
     */
    public int getBottomRightRadius() {
        return bottomRightRadius;
    }

    /**
     * getBottomLeftRadius
     *
     * @return int
     */
    public int getBottomLeftRadius() {
        return bottomLeftRadius;
    }

    /**
     * isRadiusForEverySegment
     *
     * @return boolean
     */
    public boolean isRadiusForEverySegment() {
        return radiusForEverySegment;
    }

}
