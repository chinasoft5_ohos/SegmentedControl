package segmented_control.widget.custom.ohos.com.segmentedcontrol.item_row;


import ohos.agp.components.ComponentContainer;
import ohos.agp.components.LayoutScatter;
import ohos.app.Context;

import org.jetbrains.annotations.NotNull;

import section_layout.widget.custom.ohos.com.sectionlayout.SectionLayout;
import section_layout.widget.custom.ohos.com.sectionlayout.distributive_section_layout.DistributiveSectionLayout;

import segmented_control.widget.custom.ohos.com.segmentedcontrol.item_row_column.SegmentAdapter;
import segmented_control.widget.custom.ohos.com.segmentedcontrol.item_row_column.SegmentData;

/**
 * Created by Robert Apikyan on 9/7/2017.
 */

public class SegmentRowAdapter<D> extends SectionLayout.Adapter<Boolean, SegmentRowViewHolder<SegmentData<D>>> {
    /**
     * segmentAdapter
     */
    private final SegmentAdapter segmentAdapter;

    public SegmentRowAdapter(SegmentAdapter segmentAdapter) {
        this.segmentAdapter = segmentAdapter;
    }

    @NotNull
    @Override
    protected SegmentRowViewHolder<SegmentData<D>> onCreateViewHolder(@NotNull LayoutScatter layoutInflater, Context context,ComponentContainer viewGroup, int i) {
        return new SegmentRowViewHolder<>(new DistributiveSectionLayout<SegmentData<D>>(context), segmentAdapter);
    }
}
