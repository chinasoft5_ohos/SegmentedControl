package segmented_control.widget.custom.ohos.com.segmentedcontrol.item_row;


import ohos.agp.components.Component;
import ohos.agp.components.DirectionalLayout;

import org.jetbrains.annotations.NotNull;

import section_layout.widget.custom.ohos.com.sectionlayout.SectionLayout;
import section_layout.widget.custom.ohos.com.sectionlayout.distributive_section_layout.DistributiveSectionLayout;

import segmented_control.widget.custom.ohos.com.segmentedcontrol.item_row_column.SegmentAdapter;
import segmented_control.widget.custom.ohos.com.segmentedcontrol.item_row_column.SegmentData;

/**
 * Created by Robert Apikyan on 9/7/2017.
 */

public class SegmentRowViewHolder<D> extends SectionLayout.ViewHolder<Boolean> {
    /**
     * distributiveSectionLayout
     */
    private final DistributiveSectionLayout<SegmentData<D>> distributiveSectionLayout;

    SegmentRowViewHolder(@NotNull Component sectionView, SegmentAdapter segmentAdapter) {
        super(sectionView);
        //noinspection unchecked
        distributiveSectionLayout = (DistributiveSectionLayout<SegmentData<D>>) sectionView;
        distributiveSectionLayout.setOrientation(DirectionalLayout.HORIZONTAL);
        distributiveSectionLayout.withAdapter(segmentAdapter);
    }

    @Override
    protected void onBind(Boolean willDistributeEvenly) {
        distributiveSectionLayout.distributeEvenly(willDistributeEvenly);
    }

    public DistributiveSectionLayout<SegmentData<D>> getDistributiveSectionLayout() {
        return distributiveSectionLayout;
    }
}
