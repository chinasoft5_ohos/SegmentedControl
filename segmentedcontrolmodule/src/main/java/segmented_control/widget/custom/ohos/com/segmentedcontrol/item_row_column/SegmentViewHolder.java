package segmented_control.widget.custom.ohos.com.segmentedcontrol.item_row_column;


import ohos.agp.components.Component;

import org.jetbrains.annotations.NotNull;

import section_layout.widget.custom.ohos.com.sectionlayout.distributive_section_layout.DistributiveSectionLayout;

/**
 * Created by Robert Apikyan on 9/7/2017.
 */

public abstract class SegmentViewHolder<D> extends DistributiveSectionLayout.ViewHolder<SegmentData<D>> {
    /**
     * segmentData
     */
    private SegmentData<D> segmentData;

    /**
     * onSectionViewClickListener
     */
    private final Component.ClickedListener onSectionViewClickListener = new Component.ClickedListener() {
        @Override
        public void onClick(Component component) {
            if (segmentData != null) {
                segmentData.getOnSegmentClickListener().onSegmentClick(SegmentViewHolder.this);
            }
        }
    };

    public SegmentViewHolder(@NotNull Component sectionView) {
        super(sectionView);
    }

    @Override
    protected final void onBind(SegmentData<D> segmentData) {
        this.segmentData = segmentData;
        getSectionView().setClickedListener(onSectionViewClickListener);
        onSegmentBind(segmentData.getSegmentData());
    }

    /**
     * setSelected
     *
     * @param isSelected isSelected
     */
    public final void setSelected(boolean isSelected) {
        if (segmentData.isSelected() && isSelected) {
            segmentData.isSelected = true;
            onSegmentSelected(true, true);
        } else if (isSelected) {
            segmentData.isSelected = true;
            onSegmentSelected(true, false);
        } else {
            segmentData.isSelected = false;
            onSegmentSelected(false, false);
        }
    }

    /**
     * Override this method in order to define, performed action, selected, unselected, reselected
     *
     * @param isSelected represent selected state
     * @param isReselected represent reselected state
     */
    public void onSegmentSelected(boolean isSelected, boolean isReselected) {
    }

    /**
     * getAbsolutePosition
     *
     * @return int
     */
    public int getAbsolutePosition() {
        return segmentData.absolutePosition;
    }

    /**
     * isSelected
     *
     * @return boolean
     */
    public boolean isSelected() {
        return segmentData.isSelected;
    }

    /**
     * getRow
     *
     * @return int
     */
    public int getRow() {
        return segmentData.getRow();
    }

    /**
     * getColumn
     *
     * @return int
     */
    public int getColumn() {
        return segmentData.getColumn();
    }

    /**
     * getSegmentData
     *
     * @return d
     */
    public D getSegmentData() {
        return segmentData.getSegmentData();
    }

    /**
     * getSelectedStrokeColor
     *
     * @return int
     */
    public int getSelectedStrokeColor() {
        return segmentData.getSelectedStrokeColor();
    }

    /**
     * getUnSelectedStrokeColor
     *
     * @return int
     */
    public int getUnSelectedStrokeColor() {
        return segmentData.getUnSelectedStrokeColor();
    }

    /**
     * getStrokeWidth
     *
     * @return int
     */
    public int getStrokeWidth() {
        return segmentData.getStrokeWidth();
    }

    /**
     * getSelectBackgroundColor
     *
     * @return int
     */
    public int getSelectBackgroundColor() {
        return segmentData.getSelectBackgroundColor();
    }

    /**
     * getUnSelectedBackgroundColor
     *
     * @return int
     */
    public int getUnSelectedBackgroundColor() {
        return segmentData.getUnSelectedBackgroundColor();
    }

    /**
     * getFocusedBackgroundColor
     *
     * @return int
     */
    public int getFocusedBackgroundColor(){
        return segmentData.getFocusedBackgroundColor();
    }

    /**
     * getSelectionAnimationDuration
     *
     * @return int
     */
    public int getSelectionAnimationDuration(){
        return segmentData.getSelectionAnimationDuration();
    }

    /**
     * getSelectedTextColor
     *
     * @return int
     */
    public int getSelectedTextColor() {
        return segmentData.getSelectedTextColor();
    }

    /**
     * getUnSelectedTextColor
     *
     * @return int
     */
    public int getUnSelectedTextColor() {
        return segmentData.getUnSelectedTextColor();
    }

    /**
     * getTextSize
     *
     * @return int
     */
    public int getTextSize() {
        return segmentData.getTextSize();
    }

    /**
     * getCurrentSize
     *
     * @return int
     */
    public int getCurrentSize() {
        return segmentData.getCurrentSize();
    }

    /**
     * getColumnCount
     *
     * @return int
     */
    public int getColumnCount() {
        return segmentData.getColumnCount();
    }

    /**
     * getTextHorizontalPadding
     *
     * @return int
     */
    public int getTextHorizontalPadding() {
        return segmentData.getTextHorizontalPadding();
    }

    /**
     * getTextVerticalPadding
     *
     * @return int
     */
    public int getTextVerticalPadding() {
        return segmentData.getTextVerticalPadding();
    }

    /**
     * getSegmentVerticalMargin
     *
     * @return int
     */
    public int getSegmentVerticalMargin() {
        return segmentData.getSegmentVerticalMargin();
    }

    /**
     * getSegmentHorizontalMargin
     *
     * @return int
     */
    public int getSegmentHorizontalMargin() {
        return segmentData.getSegmentHorizontalMargin();
    }

    /**
     * getTopLeftRadius
     *
     * @return int
     */
    public int getTopLeftRadius() {
        return segmentData.getTopLeftRadius();
    }

    /**
     * getTopRightRadius
     *
     * @return int
     */
    public int getTopRightRadius() {
        return segmentData.getTopRightRadius();
    }

    /**
     * getBottomRightRadius
     *
     * @return int
     */
    public int getBottomRightRadius() {
        return segmentData.getBottomRightRadius();
    }

    /**
     * getBottomLeftRadius
     *
     * @return int
     */
    public int getBottomLeftRadius() {
        return segmentData.getBottomLeftRadius();
    }

    /**
     * isRadiusForEverySegment
     *
     * @return boolean
     */
    public boolean isRadiusForEverySegment() {
        return segmentData.isRadiusForEverySegment();
    }

    protected abstract void onSegmentBind(D segmentData);
}
