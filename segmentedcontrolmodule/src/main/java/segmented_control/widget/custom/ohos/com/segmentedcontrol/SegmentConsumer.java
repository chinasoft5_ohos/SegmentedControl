package segmented_control.widget.custom.ohos.com.segmentedcontrol;


import segmented_control.widget.custom.ohos.com.segmentedcontrol.item_row_column.SegmentViewHolder;

public interface SegmentConsumer<D> {
    void apply(SegmentViewHolder<D> segmentViewHolder);
}