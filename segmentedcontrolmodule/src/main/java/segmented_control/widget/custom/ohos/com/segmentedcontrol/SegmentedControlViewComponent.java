package segmented_control.widget.custom.ohos.com.segmentedcontrol;


import ohos.agp.components.Component;
import ohos.agp.components.DirectionalLayout;

import org.jetbrains.annotations.NotNull;

import section_layout.widget.custom.ohos.com.sectionlayout.SectionLayout;

import view_component.lib_ohos.com.view_component.base_view.ViewComponent;

/**
 * Created by Robert Apikyan on 9/5/2017.
 */

class SegmentedControlViewComponent<D> extends ViewComponent {
    /**
     * verticalSectionLayout
     */
    final SectionLayout<D> verticalSectionLayout;

    SegmentedControlViewComponent(@NotNull Component rootView) {
        super(rootView);
        //noinspection unchecked
        verticalSectionLayout = (SectionLayout<D>) getRootViewGroup().getComponentAt(0);
        verticalSectionLayout.setOrientation(DirectionalLayout.VERTICAL);
    }
}
