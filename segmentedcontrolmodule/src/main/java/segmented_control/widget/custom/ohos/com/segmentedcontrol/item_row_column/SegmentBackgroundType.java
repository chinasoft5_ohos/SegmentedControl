package segmented_control.widget.custom.ohos.com.segmentedcontrol.item_row_column;

/**
 * SegmentBackgroundType
 *
 * @since 2021/06/23
 */
public @interface SegmentBackgroundType {
    /**
     * SINGLE_BG
     */
    int SINGLE_BG = 0;

    /**
     * TOP_SINGLE_BG
     */
    int TOP_SINGLE_BG = 1;

    /**
     * TOP_LEFT_BG
     */
    int TOP_LEFT_BG = 2;

    /**
     * TOP_LEFT_SINGLE_BG
     */
    int TOP_LEFT_SINGLE_BG = 3;

    /**
     * TOP_RIGHT_SINGLE_BG
     */
    int TOP_RIGHT_SINGLE_BG = 4;

    /**
     * TOP_RIGHT_BG
     */
    int TOP_RIGHT_BG = 5;

    /**
     * MIDDLE_BG
     */
    int MIDDLE_BG = 6;

    /**
     * BOTTOM_SINGLE_BG
     */
    int BOTTOM_SINGLE_BG = 7;

    /**
     * BOTTOM_LEFT_BG
     */
    int BOTTOM_LEFT_BG = 8;

    /**
     * BOTTOM_RIGHT_BG
     */
    int BOTTOM_RIGHT_BG = 9;
}
