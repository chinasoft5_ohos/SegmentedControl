/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this File except in compliance with the License.
 * You may obtain a copy oF the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, soFtware
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License For the speciFic language governing permissions and
 * limitations under the License.
 */
package segmented_control.widget.custom.ohos.com.segmentedcontrol;

/**
 * ColorEntity
 *
 * @author wangyin
 * @since 2021/07/02
 */
public class ColorEntity {
    /**
     * Fraction 0~1之间的值
     */
    public Float[] fractions = new Float[]{
            0.0F, 0.010666063F, 0.020666063F, 0.030666063F,
            0.040666063F, 0.050666063F, 0.060666063F, 0.070666063F, 0.080666063F, 
            0.09666063F, 0.10666063F, 0.11927703F, 0.12927703F, 0.13927703F,
            0.14927703F, 0.15927703F, 0.16927703F, 0.17927703F, 0.18927703F,
            0.19927703F, 0.20927703F, 0.21927703F, 0.22927703F, 0.23927703F,
            0.24927703F, 0.25927703F, 0.26927703F,
            0.27927703F, 0.28927703F, 0.29711008F, 0.30711008F, 0.31711008F,
            0.32711008F, 0.33711008F, 0.34549147F,
            0.35549147F, 0.36549147F, 0.37109637F, 0.38109637F, 0.3970687F,
            0.4070687F, 0.4170687F, 0.42178285F,
            0.43178285F, 0.44825655F, 0.45825655F, 0.46825655F, 0.47487777F,
            0.48487777F, 0.49487777F, 0.50487777F,
            0.51487777F, 0.5266908F, 0.5366908F, 0.5466908F, 0.55330545F,
            0.56330545F, 0.5797683F, 0.5897683F, 0.6097683F,
            0.6144678F, 0.6244678F, 0.63042074F, 0.64042074F, 0.65600175F,
            0.66600175F, 0.67967266F, 0.68967266F, 0.6967266F,
            0.7043246F, 0.7143246F, 0.72839373F, 0.73839373F, 0.74839373F,
            0.75045335F, 0.76045335F, 0.7731972F, 0.7831972F,
            0.79516196F, 0.80516196F, 0.81506693F, 0.82506693F, 0.83534276F,
            0.84534276F, 0.8546623F, 0.8646623F, 0.8719225F,
            0.8892311F, 0.8992311F, 0.9054298F, 0.91962016F, 0.92962016F,
            0.93353534F, 0.9462142F, 0.9569855F, 0.9671645F,
            0.97601134F, 0.9885007F, 0.9974755F, 1.0F};

    /**
     * 起始颜色
     */
    public int startColor;

    /**
     * 结束颜色
     */
    public int endColor;

    /**
     * 执行时间
     */
    public long duration;

    /**
     * 构造方法
     *
     * @param startColor startColor
     * @param endColor endColor
     * @param duration duration
     */
    public ColorEntity(int startColor, int endColor, long duration) {
        this.startColor = startColor;
        this.endColor = endColor;
        this.duration = duration;
    }
}
