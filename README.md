# SegmentedControl

### 项目介绍

- 项目名称：SegmentedControl
- 所属系列：openharmony的第三方组件适配移植
- 功能：分段控件，有很多自定义属性。
- 项目移植状态：主功能完成
- 调用差异：无
- 开发版本：sdk6，DevEco Studio2.2 Beta1
- 基线版本：Release 1.2.0

### 效果演示

   ![screen1](./printscreen/test.gif)
   
### 安装教程

1.在项目根目录下的build.gradle文件中，

```
allprojects {
    repositories {
        maven {
             url 'https://s01.oss.sonatype.org/content/repositories/releases/'
        }
    }
}
```

2.在entry模块的build.gradle文件中，

```
    implementation('com.gitee.chinasoft_ohos:segmentedcontrol:1.0.0')
```

在sdk6，DevEco Studio2.2 Beta1下项目可直接运行
如无法运行，删除项目.gradle,.idea,build,gradle,build.gradle文件，
并依据自己的版本创建新项目，将新项目的对应文件复制到根目录下

### 使用说明

1.在布局文件中加入SegmentedControl控件,代码实例如下:

```
  <segmented_control.widget.custom.ohos.com.segmentedcontrol.SegmentedControl
              ohos:height="match_content"
              ohos:width="match_content"
              ohos:segments="C语言,Java,C#,PHP,HTML"
              />

```

2.Customization

```
 <segmented_control.widget.custom.ohos.com.segmentedcontrol.SegmentedControl
             ohos:height="match_content"
             ohos:width="match_content"
             ohos:columnCount="3"
             ohos:selectionAnimationDuration="500"
             ohos:radius="12vp"
             ohos:topLeftRadius="12vp"
             ohos:topRightRadius="12vp"
             ohos:distributeEvenly="false"
             ohos:focusedBackgroundColor="#d28eff"
             ohos:margin="15vp"
             ohos:radiusForEverySegment="true"
             ohos:segments="C语言,Java,C#,PHP,HTML"
             ohos:selectedBackgroundColor="#b94fff"
             ohos:selectedStrokeColor="#b94fff"
             ohos:selectedTextColor="#ffffff"
             ohos:textVerticalPadding="6vp"
             ohos:segmentVerticalMargin="6vp"
             ohos:segmentHorizontalMargin="6vp"
             ohos:unSelectedBackgroundColor="#e8ccff"
             ohos:unSelectedStrokeColor="#b94fff"
             ohos:unSelectedTextColor="#b94fff"/>
```

### 测试信息

CodeCheck代码测试无异常

CloudTest代码测试无异常

病毒安全检测通过

当前版本demo功能与原组件基本无差异

### 版本迭代

- 1.0.0

- 0.0.1-SNAPSHOT

### 版权和许可信息

```
Copyright 2017 Robert Apikyan

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

```


